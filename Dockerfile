FROM python:3.8-slim as builder

RUN apt-get update \
    && apt-get install --no-install-recommends -y build-essential gcc \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY ./requirements.txt /requirements.txt

WORKDIR /app
RUN python -m venv /app/venv
ENV PATH="/app/venv/bin:$PATH"

RUN pip install -r /requirements.txt \
    && rm -rf /root/.cache/pip


FROM python:3.8-slim@sha256:2d397cf0f7a5f987c1ca8b0f528d6806410ab07d8dc9d8c816124e8aedff97de

COPY --from=builder /app/venv /app/venv
ENV PATH="/app/venv/bin:$PATH"

COPY ./app /app/
EXPOSE 8000

CMD ["uvicorn", "app.main:app","--reload", "--host", "0.0.0.0"]
