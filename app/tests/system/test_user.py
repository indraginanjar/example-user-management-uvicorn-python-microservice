from starlette.testclient import TestClient

from app.main import app

client = TestClient(app)

def test_get_user_returns_an_empty_json():
    response = client.get(
        "/user/",
        headers={"accept": "application/json"}
    )

    assert response.status_code == 200
    assert response.json() == {}
